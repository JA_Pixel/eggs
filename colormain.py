import cv2
import glob
import numpy as np
from EggLib import EggCup, ColorEggCup


def the_key(filename):
    in_iz = filename.rfind('capture_')
    in_der = filename.rfind('.')
    return int(filename[in_iz + len('capture_'):in_der])

#file_names = sorted(glob.glob('/home/dino/PycharmProjects/eggs_/video_huevos/images/*.jpg'), key=the_key)
file_names = sorted(glob.glob('/media/dino/5152ABAF3E76E482/VideoCapture/images/01-12-17_17-17-21/01_12_17/*.jpg'), key=the_key)


ec = ColorEggCup('The_Instalation', debug=True)
for file_name in file_names:
    ec.feed(file_name)
