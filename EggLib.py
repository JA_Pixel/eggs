import cv2
import numpy as np
from collections import deque
import random
from string import ascii_lowercase, digits
import pprint

def get_id(size=4, chars=ascii_lowercase + digits, prefix='egg_'):
    return prefix + ''.join(random.SystemRandom().choices(population=chars, k=size))


class Circle:
    def __init__(self, circle):
        self.center = (circle[0], circle[1])
        self.radius = circle[2]

    def draw(self, image, name):
        center_to_draw = (int(round(self.center[0])), int(round(self.center[1])))
        radius_to_draw = int(round(self.radius))
        #print('Center to draw', center_to_draw, 'radius to draw', radius_to_draw)
        image = cv2.circle(image, center_to_draw, radius_to_draw, (0, 255, 0), 2)

        return cv2.putText(image, name, center_to_draw, cv2.FONT_HERSHEY_DUPLEX, 0.5, (128, 0, 128))

    def distance(self, other):
        x1, y1, r1 = *self.center, self.radius
        x2, y2, r2 = *other.center, other.radius
        return ((x2 - x1) ** 2 + (y2 - y1) ** 2 + (r2 - r1) ** 2) ** 0.5


class Egg:
    def __init__(self, circle):
        # The actual first circle detected
        self.circle = circle
        # The radius of the first circle
        self.radius = self.total_radius = circle.radius
        # Number of times this egg was detected
        self.number_detections = 1
        # All detections for this egg
        self.history = [circle]
        # Last 5 movements of this circle
        self.movement = deque([(0, 0)] * 5)
        # Get a name for me
        self.name = get_id(prefix='')

        self.frames_out = 0

    @property
    def active(self):
        # Whether this egg is active or not
        #print(self.frames_out)
        return self.frames_out < 2

    def update(self, circle):
        self.frames_out = 0
        self.number_detections += 1
        self.history.append(circle)
        new_center = self._get_center(circle.center)
        new_radius = self._get_radius(circle.radius)
        self.circle = Circle((*new_center, new_radius))


    def _get_center(self, center):
        #print('Center got', center)
        last_center = self.circle.center
        this_center = center
        this_movement = (this_center[0] - last_center[0], this_center[1] - last_center[1])
        #print('Movement', this_movement)
        acc_x = acc_y = 0
        for x, y in self.movement:
            acc_x += x
            acc_y += y
        acc_x /= 5
        acc_y /= 5
        final_movement = (0.25 * acc_x + 0.75 * this_movement[0], 0.25 * acc_y + 0.75 * this_movement[1])
        center_to_show = (last_center[0] + final_movement[0], last_center[1] + final_movement[1])
        self.movement.popleft()
        self.movement.append(this_movement)
        return center_to_show

    def _get_radius(self, radius):
        self.total_radius += radius
        return self.total_radius / self.number_detections

    def draw(self, image):
        return self.circle.draw(image, self.name)

    def new_frame(self):
        self.frames_out += 1


class EggCup:
    def __init__(self, name, debug=False):
        self.name = name
        self.img = None
        self.img_canny = None
        self.eggs = []
        self.DEBUG = debug

    def feed(self, filename):
        self.img = cv2.imread(filename)

        self.img = cv2.resize(self.img, (0, 0), fx=0.5, fy=0.5)

        circles_raw = self._get_circles()
        #print('circles_raw', circles_raw)
        self._update_all_eggs(circles_raw)
        self.draw()

    def _get_circles(self):

        gray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)

        cimg = cv2.medianBlur(gray, 23)

        # p1, p2 = 20, 50

        p1, p2 = 50, 30
        self.img_canny = cv2.Canny(cimg, p1, p2)

        #print(cimg.dtype)
        circles_raw = cv2.HoughCircles(cimg, cv2.HOUGH_GRADIENT, dp=1, minDist=20,
                                       param1=p1, param2=p2, minRadius=25, maxRadius=50)

        return circles_raw

    def _update_all_eggs(self, circles_raw):
        self._notify_all()
        if circles_raw is not None:
            #circles_rounded = np.uint16(np.around(circles_raw))
            circles = [Circle(c) for c in circles_raw[0, :]]
            for egg in self.eggs:
                distance_min = float('inf')
                index2pop = -1
                for i, c in enumerate(circles):
                    distance = egg.circle.distance(c)
                    #print(i, 'distance', distance)
                    if distance < distance_min and distance < 50:
                        distance_min = distance
                        index2pop = i
                if index2pop != -1:
                    egg.update(circles.pop(index2pop))
            if len(circles):
                for c in circles:
                    self.eggs.append(Egg(c))

    def draw(self):
        to_delete = []
        for i, egg in enumerate(self.eggs):
            if egg.active:
                self.img = egg.draw(self.img)
            else:
                to_delete.append(i)
        for i in reversed(to_delete):
            self.eggs.pop(i)

        if self.DEBUG:
            self._draw_debug()
        else:
            self._draw()

        cv2.waitKey(0)


    def _draw(self):
        cv2.imshow(self.name, self.img)

    def _draw_debug(self):
        to_show = np.hstack((self.img, cv2.cvtColor(self.img_canny, cv2.COLOR_GRAY2BGR)))
        cv2.imshow(self.name, to_show)


    def _notify_all(self):
        for egg in self.eggs:
            egg.new_frame()



class ColorEggCup(EggCup):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.low = np.array([9, 85, 129])
        self.high = np.array([22, 212, 255])
        self.se = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))

    def _get_circles(self):
        mask = cv2.inRange(cv2.cvtColor(self.img, cv2.COLOR_BGR2HSV), self.low, self.high)
        mask = cv2.medianBlur(mask, 23)

        mask = cv2.morphologyEx(mask, cv2.MORPH_DILATE, self.se)

        self.img_bw = mask

        (_, ctr, _) = cv2.findContours(mask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        #a = cv2.drawContours(self.img, ctr, -1, (128, 0, 128), cv2.FILLED, cv2.LINE_4)

        circles_raw = [cv2.minEnclosingCircle(c) for c in ctr if self._rectangular_shape(c)]
        circles_raw = [[center[0], center[1], rad] for center, rad in circles_raw]

        return circles_raw

    def _draw_debug(self):
        to_show = np.hstack((self.img, cv2.cvtColor(self.img_bw, cv2.COLOR_GRAY2BGR)))
        cv2.imshow(self.name, to_show)

    def _update_all_eggs(self, circles_raw):
        self._notify_all()
        if circles_raw is not None:
            circles = [Circle(c) for c in circles_raw]
            for egg in self.eggs:
                distance_min = float('inf')
                index2pop = -1
                for i, c in enumerate(circles):
                    distance = egg.circle.distance(c)
                    #print(i, 'distance', distance)
                    if distance < distance_min and distance < 50:
                        distance_min = distance
                        index2pop = i
                if index2pop != -1:
                    egg.update(circles.pop(index2pop))
            if len(circles):
                for c in circles:
                    self.eggs.append(Egg(c))

    def _rectangular_shape(self, contour):

        M = cv2.moments(contour)

        cx, cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
        self.img = cv2.circle(self.img, (cx, cy), 1, (0, 0, 0), 2)

        (x, y, w, h) = cv2.boundingRect(contour)

        cx_, cy_ = x + w*0.5, y + h * 0.5

        d = ((cx-cx_) ** 2 + (cy-cy_) ** 2) ** 0.5


        num = abs(1 - w / (h + 0.01))

        #print('Squarelike shape closure', num)

        rectangular = abs(1 - w / (h + 0.01)) < 0.15


        area = cv2.contourArea(contour)

        rad = (w + h) * 0.25
        area2 = np.pi * (rad ** 2)
        circular = 1 - abs(area - area2)/area2

        if not rectangular or circular < 0.95:
            print('Distance from centroid {:.2f} NOT A RECT'.format(d))
            return False
        print('Distance from centroid {:.2f}'.format(d))
        return True
