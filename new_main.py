import cv2
import glob
import numpy as np
from EggLib import EggCup, ColorEggCup


def the_key(filename):
    in_iz = filename.rfind('frame')
    in_der = filename.rfind('.')
    return int(filename[in_iz + len('frame'):in_der])

file_names = sorted(glob.glob('video_huevos/images/*.jpg'), key=the_key)
ec = ColorEggCup('The_Instalation', True)
#ec = EggCup('The_Instalation', True)
for file_name in file_names:
    ec.feed(file_name)
